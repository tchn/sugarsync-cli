##Setup

> $ cd sugarsync-cli

> $ pip install --user -r requirements.txt

##Usage

> $ sugarsync-client.py [-h] [--debug] {init,user,ls,download,upload} ...

* Command:

    * init (Initialize this client)
    * user (Retrieve info about the user)
    * ls   (List the contents of the folder)
    * download (Download remote file)
    * upload (Upload named file)

* optional arguments:

    * -h, --help            show this help message and exit

    * --debug               Run with pdb trace

##Restrictions
* Python 3 compatibility is a ToDo.
* Certain version of gnome-keyring may not work well with python keyring. In such case, use python keyring installed via apt may work better.
* Lots of ToDos.
#!/usr/bin/env python

from __future__ import print_function
import keyring
import argparse
import os
import sys
import getpass
import xml.etree.ElementTree as ElementTree
from huTools import structured
import requests
import json
import xmltodict
import magic

# on high-level
## 0. parse arguments
## 1. if first run, prompt user to register app with sugarsync.

## init
## 2. let user input appname and API access keys
## 3. get refresh token
## 4. get access token

## 5. access API that performs the user's request
### 5.1 user
### 5.1.1 request to /user
### 5.1.2 display
###
### 5.2 ls
### 5.2.1 user
### 5.2.2 get webArchive ref
### 5.2.3 get webArchive contents
### 5.2.4 display

### 5.3 download
### 5.3.1 steps to 5.2.3
### 5.3.2 get target ref by user input displayName
### 5.3.3 request the file and locally saves the stream

### 5.4 upload
### 5.4.1 steps to 5.2.3
### 5.4.2 get file ref
### 5.4.2.1 if overwriting, use existing ref
### 5.4.2.2 if new file name, create file on the remote and get ref
### 5.4.3 get mime
### 5.4.4 upload the source file to the remote file ref

origin = 'https://api.sugarsync.com'

class DataDescriptor( object ):
    """
    Descriptor class to be used in other classes
    """
    def __init__( self, attr ):
        self.attr = attr

    def __get__( self, instance, instance_type ):
        return getattr( instance, self.attr )

    def __set__( self, instance, value ):
        setattr( instance, self.attr, value )

class XmlBuilder( object ):
    """
    Class to build XML to send data in request
    """
    xml_decl_str = '<?xml version="1.0" encoding="UTF-8"?>'

    def __init__( self, root_nodetag='data' ):
        self.root_nodetag = root_nodetag

    def build_xml( self, elem_dict ):
        xmlstr = self.xml_decl_str + structured.dict2xml( elem_dict, self.root_nodetag )
        return xmlstr

    root_nodetag = DataDescriptor( '_root_nodetag' )


class CredentialManager( object ):
    """
    Credential management class.

    cm = CredentialManager()
    cm.attr
    cm.attr = val
    cm.set_attrs( {'key1':'val1', ...} )
    """
    def __init__( self, uname=None, passwd=None, appid=None, access_key_id=None, private_access_key=None, refresh_token=None, access_token=None ):
        self.uname = uname
        self.passwd = passwd
        self.appid = appid
        self.access_key_id = access_key_id
        self.private_access_key = private_access_key
        self.refresh_token = refresh_token
        self.access_token = access_token
        self.usernum = ''

    def set_attrs( self, dictionary ):
        """
        cmgr.set_attrs( {'key':'val', ...} )

        Setter to set values to multiple attributes at once.
        """
        for key in dictionary:
            self.__dict__[key] = str( dictionary[key] )

    def get_values( self, iterable ):
        """
        cmgr.get_values ( iterable ) -> dict
        """
        d = {}
        for i in iterable:
            if i in self.__dict__:
                d[i] = self.__dict__[i]
        return d

    uname = DataDescriptor( 'username' )
    passwd = DataDescriptor( 'password' )
    appid = DataDescriptor( 'application' )
    access_key_id = DataDescriptor( 'accessKeyId' )
    private_access_key = DataDescriptor( 'privateAccessKey' )
    refresh_token = DataDescriptor( 'refreshToken' )
    access_token = DataDescriptor( 'accessToken' )
    usernum = DataDescriptor('user')

def parse_args():
    """
    parse_args() -> namespace object containing parsed arguments

    Parses arguments and returns namespace object populated with args.
    """
    parser = argparse.ArgumentParser( description='Sugarsync CLI client', conflict_handler='error', add_help='True' )
    parser.add_argument('--debug', action='store_true', dest='debug', help='Run with pdb trace' )
    subparsers = parser.add_subparsers( title='Command', dest='action' )
    subparsers.add_parser( 'init', help='Initialize this client' )
    subparsers.add_parser( 'user', help='Retrieve info about the user' )
    subparsers.add_parser( 'ls', help='List the contents of the folder' )
    parser_dl = subparsers.add_parser( 'download' , help='Download remote file' )
    parser_dl.add_argument( 'source' )
    parser_dl.add_argument( '-d', '--dest', type=str, dest='destdir', default='.', help='download destination directory, defaults to CWD.' )
    parser_upld = subparsers.add_parser( 'upload', help='Upload named file' )
    parser_upld.add_argument( 'source' )
    parser_upld.add_argument( '-o', '--overwrite', action='store_true', help='overwrite if file with same name exists.' )
    parser_del = subparsers.add_parser( 'delete', help='Delete remote file')
    parser_del.add_argument( 'targetfile' )
    args = parser.parse_args()
    return args

def register( jsonfilepath, cmgr, xmlb ):
    """
    register( filepath, CredentialManager, XmlBuilder )

    Prompts user to register app with SugarSync API, tests user credentials, and saves them if valid.
    """
    print( 'If you run this program for the first time, you need to register you app with SugarSync through the following steps.\n' )
    print( '1. Visit the Developer Console at the following URL by your browser:' )
    print( '   https://www.sugarsync.com/developer/account\n' )
    print( '2. Add Keys' )
    print( '3. Create App\n' )
    ans = raw_input( 'When complete, hit any key to proceed.' )

    print( '\nNow to set up this program to work as your newly-created SugarSync app.' )
    print( 'To save you from entering keys and id every time, they will be put in the following file this program uses. ' )
    print( '%s' % jsonfilepath )
    print( 'Password is stored in python keyring, and not in the file. ' )
    ans = raw_input( 'OK to proceed? [y/n] ' )
    if ans != 'y':
        sys.exit()

    while True:
        cmgr.uname = getpass.getpass( 'Input Username then hit enter: ' )
        cmgr.passwd = getpass.getpass( 'Input password then hit enger: ' )
        cmgr.appid = getpass.getpass( 'Input App ID then hit enter: ' )
        cmgr.access_key_id = getpass.getpass( 'Input Access Key ID then hit enter: ' )
        cmgr.private_access_key = getpass.getpass( 'Input Private Access Key then hit enter: ')

        refresh_token = get_refresh_token( cmgr, xmlb )
        if refresh_token == '':
            print( 'Refresh Token not issued.\nCredentials you input may be wrong, make sure the info is correct.' )
        else:
            break

    cmgr.refresh_token = refresh_token
    store_credentials( jsonfilepath, cmgr )

def get_refresh_token( cmgr, xmlb ):
    """
    get_refresh_token( CredentialManager, XmlBuilder ) -> string

    Creates refresh token.
    """
    endpoint = origin + '/app-authorization'
    req_elems = ('username', 'password', 'application', 'accessKeyId', 'privateAccessKey' )
    elem_dict = cmgr.get_values( req_elems )
    xmlb.root_nodetag = 'appAuthorization'
    xmldata = xmlb.build_xml( elem_dict )
    refresh_token = ''
    res = requests.post( endpoint, data=xmldata )
    if is_success( res ) and res.headers['Location'] is not None:
        refresh_token = res.headers['Location'].split( '/' )[-1]
    return refresh_token

def get_access_token( cmgr, xmlb ):
    """
    get_access_token( CredentialManager ) -> string

    Creates access token, and sets usernum to CredentialManager
    """
    endpoint = origin + '/authorization'
    req_elems = ( 'accessKeyId', 'privateAccessKey', 'refreshToken' )
    elem_dict = cmgr.get_values( req_elems )
    xmlb.root_nodetag = 'tokenAuthRequest'
    xmldata = xmlb.build_xml( elem_dict )
    access_token = ''
    res = requests.post( endpoint, data=xmldata )
    if is_success( res ) and res.headers['Location'] is not None:
        access_token = res.headers['Location'].split( '/' )[-1]
        res_dict = xmltodict.parse( res.content )
        cmgr.usernum = res_dict['authorization']['user'].split( '/' )[-1]
    return access_token

def store_credentials( jsonfilepath, cmgr ):
    """
    store_credentials( filepath, CredentialManager )

    Saves credentials to json file under the user home directory.
    """
    store_elems = ( 'username', 'accessKeyId', 'application', 'privateAccessKey' )
    elem_dict = cmgr.get_values( store_elems )
    keyring.set_password( 'sugarsync-client', cmgr.uname, cmgr.passwd )
    fd = os.open( jsonfilepath, os.O_RDWR | os.O_CREAT, 0o600 )
    open_file = os.fdopen( fd, 'w' )
    open_file.write( json.dumps( elem_dict ) )
    open_file.close()

def load_credentials( jsonfilepath, cmgr ):
    """
    load_credentials( filepath, CredentialManager ) -> CredentialManager

    Reads credentials from local file and keyring, and returns updated CredentialManager instance.
    """
    fp = open( jsonfilepath )
    dictionary = json.load( fp )
    cmgr.set_attrs( dictionary )
    cmgr.passwd = str( keyring.get_password( 'sugarsync-client', cmgr.uname ) )
    return cmgr

def is_first_run( jsonfilepath ):
    """
    is_first_run( filepath ) -> Bool

    Rudimentarily checks if user already registered this app with SugarSync
    """
    if os.path.exists( jsonfilepath ):
        return False
    else:
        return True

def get_authheader( access_token ):
    """
    get_authheader( string ) -> dict

    Returns Authorization header dict to be passed to requests
    """
    auth_header = { 'Authorization': origin + '/authorization/' + access_token }
    return auth_header

def is_success( response ):
    """
    is_success( int ) -> Bool

    Checks if HTTP Status Code of the response object is in the range of 200-299, in which case returns True.
    """
    if response.status_code >= 200 and ( response.status_code - 200 ) < 100:
        return True
    else:
        return False

def retrieve_userinfo( user, access_token, xmlb ):
    """
    retrieve_userinfo( string, string, XmlBuilder ) -> string

    Retrieve user information transfered in xml and returns it as string
    """
    endpoint = origin + '/user/' + user
    res = requests.get( endpoint, headers=get_authheader( access_token ) )
    if is_success( res ):
        return res.content

def get_rootref( xmlstr, targetnode ):
    """
    get_rootref( string, string ) -> string

    Returns text contents of the target node from XML in string representation.
    """
    elems = ElementTree.fromstring( xmlstr )
    ref = elems.findtext( targetnode )
    return ref

def get_et_list( ref, access_token, resource_type ):
    """
    get_et_list( string, string, string ) -> list

    Returns list of ElementTree of specified resource_type i.e., file, folder, etc.
    """
    endpoint = ref + '/contents?type=' + resource_type
    res = requests.get( endpoint, headers=get_authheader( access_token ) )
    if is_success( res ):
        collection_et = ElementTree.fromstring( res.content )
        file_et_list = collection_et.findall( resource_type )
        return file_et_list

def get_dict_from_et( et, exclude_node=[] ):
    """
    get_dict_from_et ( elementTree ) -> dict

    Creates dictionary from ElementTree
    et.tag as key and et.text as value
    """
    dictionary = dict()
    for elem in et.iter():
        if elem.text is not None and elem.tag not in exclude_node:
            dictionary[elem.tag] = elem.text
    return dictionary

def get_dict_of_namedkey( et_list, target_node ):
    """
    get_dict_of_namedkey( elementTree, string ) -> dict

    Creates dictionary with target_node tag as key.
    """
    ret_dict = dict()
    for et in et_list:
        key = et.findtext( target_node )
        ret_dict.setdefault( key, get_dict_from_et( et, exclude_node=[target_node] ) )
    return ret_dict

def get_mimetype( filepath ):
    """
    get_mimetype( string ) -> string

    Returns given file MIME type
    """
    mime = magic.Magic( mime=True )
    mime_str = mime.from_file( filepath )
    return mime_str

def main(args):
    if args.debug == True: import pdb; pdb.set_trace()
    jsonfilepath = os.path.expanduser( '~' ) + '/.sugarsync_cli.json'
    cmgr = CredentialManager()
    xmlb = XmlBuilder()

    if args.action == 'init' or is_first_run(jsonfilepath):
        register(jsonfilepath, cmgr, xmlb )
    else:
        load_credentials( jsonfilepath, cmgr )
        cmgr.refresh_token = get_refresh_token( cmgr, xmlb )

    cmgr.access_token = get_access_token( cmgr, xmlb )
    userinfo_str = retrieve_userinfo( cmgr.user, cmgr.access_token, xmlb )

    # user
    if args.action == 'user':
        ## ToDo: format and display
        print( userinfo_str )
        sys.exit( 0 )

    rootref = get_rootref( userinfo_str, 'webArchive' )
    resource_et_list = get_et_list( rootref, cmgr.access_token, 'file' )

    # ls
    if args.action == 'ls':
        for file_et in resource_et_list:
            ## ToDo: format and display
            print( get_dict_from_et( file_et ) )
            sys.exit( 0 )

    fname_dict = get_dict_of_namedkey( resource_et_list, 'displayName' )

    # download
    if args.action == 'download':
        dlurl = fname_dict[args.source]['fileData']
        res = requests.get( dlurl, headers=get_authheader( cmgr.access_token ), stream=True )

        ## downloads and write.
        if os.path.isdir( args.destdir ):
            dirpath = os.path.normpath( args.destdir )
            ## ToDo: use progress bar
            with open( dirpath + '/' + args.source, 'wb' ) as fp:
                fp.write( res.content )
                print( ' > DONE' )
                sys.exit( 0 )

    # upload
    if args.action == 'upload':
        if fname_dict.has_key( args.source ):
            if args.overwrite is False:
                print( 'File with same name exists. Use -o to overwrite.' )
                sys.exit( 1 )
            else:
                print( 'overwriting %s' % os.path.basename( args.source ) )
                fileref = fname_dict[ os.path.basename( args.source ) ]['ref']
                mime_str = get_mimetype( args.source )
        else:
            mime_str = get_mimetype( args.source )

            ## build xml
            elem_dict = dict()
            elem_dict.setdefault( 'mediaType', mime_str )
            elem_dict.setdefault( 'displayName', os.path.basename( args.source ) )
            file_xmlb = XmlBuilder( 'file' )
            file_xml_str = file_xmlb.build_xml( elem_dict )

            ## throw request to create file
            res = requests.post( rootref, headers=get_authheader( cmgr.access_token ), data=file_xml_str )
            if is_success( res ) and res.headers['Location'] is not None:
                fileref = res.headers['Location']

        with open( os.path.basename( args.source ) ) as upf:
            res = requests.put( fileref + '/data', headers=get_authheader( cmgr.access_token ), data=upf )
            if is_success( res ):
                print( ' > DONE' )
                sys.exit( 0 )
                ##ToDo: show progress bar

    # delete
    if args.action == 'delete':
        if fname_dict.has_key( args.targetfile ):
            fileref = fname_dict[ args.targetfile ]['ref']
            res = requests.delete( fileref, headers=get_authheader( cmgr.access_token ) )
            if is_success( res ):
                print( ' > DONE' )
                sys.exit( 0 )
        else:
            print( 'No such file' )
            sys.exit( 1 )

if __name__ == '__main__':
    main( parse_args() )
